/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.validador.CelularValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carloaiza
 */
public class ArbolABB implements Serializable {

    private NodoABB raiz;
    private int cantidadNodos;

    public ArbolABB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    ///Adicionar en el árbol
    public void adicionarNodo(Celular dato) throws CelularExcepcion {

        CelularValidador.validarDatos(dato);
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo,raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws CelularExcepcion {
        if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) == 0) {
            throw new CelularExcepcion("Ya existe un celular con el imei "
                    + nuevo.getDato().getImei());
        } else if (nuevo.getDato().getImei().compareTo(pivote.getDato().
                getImei()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha()== null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }
    
    
     public List<Celular> recorrerInOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerInOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerInOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(),listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(),listado);
        }
    }
    
    
     public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerPreOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerPreOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(),listado);            
            recorrerPreOrden(reco.getDerecha(),listado);
        }
    }
    
     public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerPostOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            
            recorrerPostOrden(reco.getIzquierda(),listado);            
            recorrerPostOrden(reco.getDerecha(),listado);
            listado.add(reco.getDato());
        }
    }
    
    public boolean esVacio()
    {
        return raiz==null;
    }
    
        //Borrar el que sea
    public String borrar(String imeiBorrar) {
        if (!this.buscar(imeiBorrar)) {
            return " ";
        }

        NodoABB z = borrar(this.raiz, imeiBorrar);
        this.setRaiz(z);
        return imeiBorrar;

    }

    private NodoABB borrar(NodoABB r, String imeiborrar) {
        if (r == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = ((Comparable) r.getDato().getImei()).compareTo(imeiborrar);
        if (compara > 0) {
            r.setIzquierda(borrar(r.getIzquierda(), imeiborrar));
        } else if (compara < 0) {
            r.setDerecha(borrar(r.getDerecha(), imeiborrar));
        } else {
            System.out.println("Encontro el dato:" + imeiborrar);
            if (r.getIzquierda() != null && r.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoABB cambiar = buscarMin(r.getDerecha());
                Celular aux = cambiar.getDato();
                cambiar.setDato(r.getDato());
                r.setDato(aux);
                r.setDerecha(borrar(r.getDerecha(), imeiborrar));
                System.out.println("2 ramas");
            } else {
                r = (r.getIzquierda() != null) ? r.getIzquierda() : r.getDerecha();
                System.out.println("Entro cuando le faltan ramas ");
            }
        }
        return r;
    }
    
        //buscar
    public boolean buscar(String imeiBorrar) {
        return (buscar(this.raiz, imeiBorrar));


    }

    private boolean buscar(NodoABB r, String imeiBorrar) {
        if (r == null) {
            return (false);
        }
        int compara = ((Comparable) r.getDato().getImei()).compareTo(imeiBorrar);
        if (compara > 0) {
            return (buscar(r.getIzquierda(), imeiBorrar));
        } else if (compara < 0) {
            return (buscar(r.getDerecha(), imeiBorrar));
        } else {
            return (true);
        }
    }
    
        //buscar min
    private NodoABB buscarMin(NodoABB r) {
        for (; r.getIzquierda() != null; r = r.getIzquierda());
        return (r);
    }
    

}
