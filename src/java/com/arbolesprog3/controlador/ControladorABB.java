/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.ArbolABB;
import com.arbolesprog3.modelo.Celular;
import com.arbolesprog3.modelo.Marca;
import com.arbolesprog3.modelo.NodoABB;
import com.arbolesprog3.modelo.Operador;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private boolean verRegistrar = false;
    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular = new Celular();
    private String imeiBorrar;
    private ArbolABB arbol = new ArbolABB();
    private String OperadorListar;

    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorABB() {
    }

    public String getOperadorListar() {
        return OperadorListar;
    }

    public void setOperadorListar(String OperadorListar) {
        this.OperadorListar = OperadorListar;
    }

    public String getImeiBorrar() {
        return imeiBorrar;
    }

    public void setImeiBorrar(String imeiBorrar) {
        this.imeiBorrar = imeiBorrar;
    }

    public ArbolABB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolABB arbol) {
        this.arbol = arbol;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    @PostConstruct
    private void iniciar() {
        llenarMarcas();
        llenarOperadores();
        pintarArbol();

    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbol(NodoABB reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato().getImei());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 8);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 8);
        }
    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));

    }

    public void guardarCelular() {
        try {
            arbol.adicionarNodo(celular);
            celular = new Celular();
            verRegistrar = false;
            pintarArbol();
            listaImeis();
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void eliminarNodo() {
        arbol.borrar(imeiBorrar);
        pintarArbol();
    }

    public List<String> listaImeis() {
        List<Celular> listadoCelulres = arbol.recorrerInOrden();
        List<String> listaImeis = new ArrayList<>();
        for (Celular cel : listadoCelulres) {
            listaImeis.add(cel.getImei());
        }
        return listaImeis;
    }

    public List<Celular> listaXOperador() {

        List<Celular> listaCelulares = new ArrayList<>();
        List<Celular> listadoCelulresXOperador = arbol.recorrerInOrden();
        for (Celular cel : listadoCelulresXOperador) {
            if (OperadorListar.equals(cel.getOperador().getNombre())) {
                listaCelulares.add(cel);
            }
        }
        return listaCelulares;
    }
}
